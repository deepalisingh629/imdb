'use strict';
const cassandra = require('cassandra-driver');
const client = new cassandra.Client({ contactPoints: ['127.0.0.1'], keyspace: 'simple' });

var util = require('util');


module.exports = {
  addMovie,
  deleteMovie,
  updateMovie
};


function addMovie(req, res) {
  var name=req.body.name;
  var actor=req.body.actor;
  var director=req.body.director;
  var producer=req.body.producer;
  console.log('name =' + name);
  function checkFields()
  {
    return new Promise((resolve,reject)=>{
      if(name!= undefined && name.length>0){
        resolve();
      } 
      else{
        reject('dont u know the name of movie?');
      }
    })
  }

  function addMovieFunction()
  {
    return new Promise((resolve,reject)=>{
      var a1="insert INTO movies (name , actor , director , producer ) values(" +"'" + name+"'"
      +","+ "'"+ actor+"'" +","+"'"+ director+"'" +","+"'"+ producer+"'"+")";
       client.execute(a1)
       .then(res1=>{
         resolve();
       })
       .catch(err=>{
         console.log("error in adding data" + err);
         reject("sorry i am in sleepy mood"); 
       })
    })
  }

  function sendResponse()
  {
    return new Promise((resolve,reject)=>{
    res.json('success');
    })
  }

  checkFields()
  .then(addMovieFunction)
  .then(sendResponse)
  .catch(err=>{
    console.log('error is ' + err);
    res.json(err);
  })
}


function deleteMovie(req, res) {
  var name=req.body.name;
  var actor=req.body.actor;
  var director=req.body.director;
  var producer=req.body.producer;
  console.log('name =' + name);
  function checkFields()
  {
    return new Promise((resolve,reject)=>{
      if(name!= undefined && name.length>0){
        resolve();
      } 
      else{
        reject('dont u know the name of movie?');
      }
    })
  }

  function deleteMovieFunction()
  {
    return new Promise((resolve,reject)=>{
      var a1=" delete from movies where name=" +"'" + name +"'";

       client.execute(a1)
       .then(res1=>{
         resolve();
       })
       .catch(err=>{
         console.log("error in deleting data" + err);
         reject("sorry i am in sleepy mood"); 
       })
    })
  }



  function sendResponse()
  {
    return new Promise((resolve,reject)=>{
    res.json('success');
    })
  }

  checkFields()
  .then(deleteMovieFunction)
  .then(sendResponse)
  .catch(err=>{
    console.log('error is ' + err);
    res.json(err);
  })
}

function updateMovie(req, res) {
  var name=req.body.name;
  var actor=req.body.actor;
  var director=req.body.director;
  var producer=req.body.producer;
  console.log('name =' + name);
  function checkFields()
  {
    return new Promise((resolve,reject)=>{
      if(name!= undefined && name.length>0){
        resolve();
      } 
      else{
        reject('dont u know the name of movie?');
      }
    })
  }

  function updateMovieFunction()
  {
    return new Promise((resolve,reject)=>{
      var a1="insert INTO movies (name , actor , director , producer ) values(" +"'" + name+"'"
      +","+ "'"+ actor+"'" +","+"'"+ director+"'" +","+"'"+ producer+"'"+")";
       client.execute(a1)
       .then(res1=>{
         resolve();
       })
       .catch(err=>{
         console.log("error in updating data" + err);
         reject("sorry i am in sleepy mood"); 
       })
    })
  }

  function sendResponse()
  {
    return new Promise((resolve,reject)=>{
    res.json('success');
    })
  }

  checkFields()
  .then(updateMovieFunction)
  .then(sendResponse)
  .catch(err=>{
    console.log('error is ' + err);
    res.json(err);
  })
}